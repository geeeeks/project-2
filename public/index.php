<?php

require_once '../lib/database.php';
require_once '../lib/bootstrap.php';
require_once '../lib/library.php';
require_once '../controller/index.php';

use Bootstrap\Bootstrap as Bootstrap;
use Database\Database as DB;

Bootstrap::run(realpath(dirname(__FILE__ ) ));
DB::configure(
	'localhost',
	'root',
	'',
	'train'
);
(new Bootstrap())->set_up_application();