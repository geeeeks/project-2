<?php

namespace AjaxHook;

require_once __DIR__ . '/../inc/index.php';


class AjaxHook
{

	public $allAjax = array();

	public function init()
	{
		if (array_key_exists('action', $_REQUEST) ) {
			$getAction = $_REQUEST['action'];
			$this->allAjax[$getAction]();
		}
	}

	public function addAdminAjaxAction($key, $fn)
	{

		$this->allAjax[$key] = $fn;

	}

}