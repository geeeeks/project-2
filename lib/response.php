<?php

namespace Response;

class Response {

	public static function json($message, $code = 200) {

		http_response_code($code);

		$message['status'] = $code;

		echo json_encode((object) $message, JSON_FORCE_OBJECT);

		die();

	}

}