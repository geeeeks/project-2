<?php 

namespace Request;

class Request {


	public static function get($param) 
	{
		if ( array_key_exists($param, $_REQUEST) ) {
			return $_REQUEST[$param];	
		}

		return null;
		
	}

	public static function all()
	{
		if ( count($_REQUEST) ) {
			return $_REQUEST;
		}

		return null;
	}

}