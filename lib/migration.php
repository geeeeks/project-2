<?php

namespace Migration;

require_once  __DIR__ . '/database.php';
use Database\Database as DB;


class Migration
{

	public static function createMigration()
	{
		self::createAllUsersTable();
		self::createUserTable();

	}

	public function createAllUsersTable()
	{
		$tableName = 'all_users';
		if(! DB::tableExists( $tableName )) {
			$sql = "CREATE TABLE $tableName (
					`id` int (11) AUTO_INCREMENT PRIMARY KEY,
					`nid` VARCHAR(100) NOT NULL,
					`user_name` VARCHAR(50) NOT NULL
			)";

			DB::runSQL($sql);
		}
	}
	public function createUserTable() 
	{
		$tableName = 'user';
		if(! DB::tableExists( $tableName)){
			$sql = "CREATE TABLE $tableName(
					`id` int(11) AUTO_INCREMENT PRIMARY KEY,
					`nid` VARCHAR(100) NOT NULL,
					`user_email` VARCHAR(50) NOT NULL,
					`user_name` VARCHAR(50) NOT NULL,
					`password` VARCHAR(100) NOT NULL
			)";
			DB::runSQL($sql);
		}
	}
	public function createMigrateTable()
	{
		$tableName = 'migration';

		if (! DB::tableExists( $tableName )) {
			$sql = "CREATE TABLE $tableName (
					`id` int(11) AUTO_INCREMENT PRIMARY KEY,
					`key` VARCHAR(50) NOT NULL,
					`value` VARCHAR(50) NOT NULL 
			)";

			DB::runSQL($sql);
		}
	}
	public function createTrainsTable()
	{
		$tableName = 'trains';
		if (! DB::tableExists( $tableName )) {
			$sql = "CREATE TABLE $tableName (
					`id` int(11) AUTO_INCREMENT PRIMARY KEY,
					`train_name` VARCHAR(50) NOT NULL,
					`status` ENUM('active','inactive') 
			)";

			DB::runSQL($sql);
		}
	}

	public static function run()
	{
		self::createMigrateTable();
		if( DB::register_hook() ) {
			self::createMigration();
			self::stopNextMigrate();		
		}
	}
}



