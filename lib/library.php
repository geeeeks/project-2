<?php

session_start();
require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/database.php';
require_once __DIR__ . '/request.php';
require_once __DIR__ . '/response.php';

use Bootstrap\Bootstrap as Bootstrap;

// _e
if (! function_exists('_e') ) {
	function _e($param) {
		echo $param;
	}
}

// home url
if (! function_exists('home_url') ) {
	function home_url($param = '') {

		return (array_key_exists('REQUEST_SCHEME', $_SERVER) ? $_SERVER['REQUEST_SCHEME'] : 'http') . '://' . $_SERVER['HTTP_HOST'] . $param;
	}
}

// current url
if (! function_exists('current_url') ) {
	function current_url() {
		return  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	}
}

// ajax url
if (! function_exists('ajax_url') ) {
	function ajax_url() {
		return (array_key_exists('REQUEST_SCHEME', $_SERVER) ? $_SERVER['REQUEST_SCHEME'] : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/ajax/';	
	}
}

// db
if (! function_exists('qFluent') ) {
	function qFluent() {
		static $qFluent;
		if (! $qFluent) {
			$qFluent = new \Database\Database();
		}
		return $qFluent;
	}
}

// request
if (! function_exists('request') ) {
	function request() {
		static $request;
		if (! $request ) {
			$request = new \Request\Request();
		}
		return $request;
	}
}

// response
if (! function_exists('response') ) {
	function response() {
		static $response;
		if (! $response ) {
			$response = new \Response\Response();
		}
		return $response;
	}
}

// admin header
if (! function_exists('admin_header') ) {
	function admin_header() {
		include((new Bootstrap())->admin_path() . '/admin-header.php');
	}
}

// admin footer
if (! function_exists('admin_footer') ) {
	function admin_footer() {
		include((new Bootstrap())->admin_path() . '/admin-footer.php');
	}
}

// home header
if (! function_exists('home_header') ) {
	function home_header() {
		include((new Bootstrap())->home_path() . '/home-header.php');
	}
}

// home footer
if (! function_exists('home_footer') ) {
	function home_footer() {
		include((new Bootstrap())->home_path() . '/home-footer.php');
	}
}

// main header
if (! function_exists('main_header') ) {
	function main_header() {
		include((new Bootstrap())->main_path() . '/main_header.php');
	}
}

// main footer
if (! function_exists('main_footer') ) {
	function main_footer() {
		include((new Bootstrap())->main_path() . '/main_footer.php');
	}
}

// assets
if (! function_exists('assets') ) {
	function assets($path) {
		return _e ( (new Bootstrap())->platformSlashes(home_url()  . '/assets/'. $path) );
	}
}

// user login session
if (! function_exists('user_login_session') ) {
	function user_login_session($user_id) {
		$_SESSION['login_session_id'] = session_id();
		$_SESSION['user_nid'] = $user_id;
	}
}

// admin login session
if (! function_exists('admin_login_session') ) {
	function admin_login_session($user_id) {
		$_SESSION['admin_login_session'] = session_id();
		$_SESSION['admin_nid'] = $user_id;
	}
}

// validate session
if (! function_exists('validate_session') ) {
	function validate_session() {
		if(user_logged_in()) {
			header('Location: ' . home_url('/home'));
		} else if(admin_logged_in()) {
			header('Location: ' . home_url('/admin'));
		} else {
			//header('Location: ' . home_url());
		}
	}
}

// get current user nid
if (! function_exists('get_current_user_nid') ) {
	function get_current_user_nid() {
		if ( array_key_exists('user_nid', $_SESSION) ) {
			return $_SESSION['user_nid'];
		} else if( array_key_exists('admin_nid', $_SESSION) ) {
			return $_SESSION['admin_nid'];
		} else {
			return 0;
		}
	}
}

if (! function_exists('current_user') ) {
	function current_user() {

		if ( get_current_user_nid()) {

			$getUser = qFluent()->table('user')->find(get_current_user_nid());
			return $getUser;
		}

		return 0;

	}
}

// check user logged in or not
if (! function_exists('user_logged_in') ) {
	function user_logged_in() {
		if ( array_key_exists('login_session_id', $_SESSION) ) {
			return 1;
		} else {
			return 0;
		}
	}
}


// check admin logged in or not
if(! function_exists('admin_logged_in') ) {
	function admin_logged_in() {
		if (array_key_exists('admin_login_session', $_SESSION) ) {
			return 1;
		} else {
			return 0;
		}
	}
}

// user logout session
if (! function_exists('user_log_out_session') )  {
	function user_log_out_session() {
		session_unset();
		session_destroy();
	}
}

// redirect url
if (! function_exists('q_redirect') ) {
	function q_redirect($route) {
		header('Location: ' . home_url() . $route);
	}
}

// if (! function_exists('route_checking') ) {
// 	function route_checking($current_url){
// 		echo

// 	}
// }
// define("QUBAXIS","HELLO WORLD");
// echo QUBAXIS;

// function shuvo($key,$value){
// 	echo $value;
// }

// shuvo("hello",QUBAXIS);

// die();
if( !function_exists('sanitize_titles')){
	function sanitize_titles($title){
		return $title = filter_var($title, FILTER_SANITIZE_STRING);
	}
}
if (! function_exists('sanitize_email') ) {
	function sanitize_email($email) {
    	return $email = filter_var($email, FILTER_SANITIZE_EMAIL);
	}
}

if (! function_exists('sanitize_text_field') ) {
	function sanitize_text_field( $str ) {
		return	$str = filter_var($str,FILTER_SANITIZE_STRING);
	}
}

if (! function_exists('sanitize_int') ) {
	function sanitize_int($int) {
		return $int = filter_var($int,FILTER_SANITIZE_NUMBER_INT);
	}
}

if (! function_exists('sanitize_float') ) {
	function sanitize_float($float) {
		return $float = filter_var($float,FILTER_SANITIZE_NUMBER_FLOAT);
	}
}
if (! function_exists('sanitize_user') ) {
	function sanitize_user($user) {
		return $user = filter_var($user,FILTER_SANITIZE_STRING);
	}
}