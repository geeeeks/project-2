<?php

namespace Bootstrap;

require_once __DIR__ . '/migration.php';
require_once __DIR__ . '/database.php';
use Database\Database as DB;
use Migration\Migration as Migration;

class Bootstrap {

	public static $app_path = null;

	public static function run($path)
	{
		$public = str_replace('/', DIRECTORY_SEPARATOR, '/public');
		self::$app_path = str_replace($public, '', $path);
	}

	public function platformSlashes($path) {
    	return str_replace('/', DIRECTORY_SEPARATOR, $path);
	}

	public function request_uri()
	{
		$uri = $_SERVER['REQUEST_URI'];

		$testUri = explode('?', $uri);

		if ( count($testUri) > 1) {
			$uri = $testUri[0];
		}

		if($uri[strlen($uri) - 1] == '/') {
			return $this->platformSlashes(self::$app_path . $uri  . 'index.php');
		} else {
			return $this->platformSlashes(self::$app_path . $uri  . '/index.php');
		}
	}

	public function set_up_application()
	{
		if ( $_SERVER['REQUEST_URI'] == '/' ) {
			include($this->platformSlashes(self::$app_path . '/main/'  . 'index.php'));
		} else if(strpos($_SERVER['REQUEST_URI'] , '/assets/') !== false) {

		} else if(strpos($_SERVER['REQUEST_URI'] , '/ajax/') !== false) { 
			include($this->platformSlashes(self::$app_path . '/ajax/'  . 'index.php'));
		}else {
			include($this->request_uri());
		}

		Migration::run();
		//print_r(DB::register_hook());
	}

	public function admin_path()
	{
		return self::$app_path . '/admin';
	}

	public function home_path()
	{
		return self::$app_path . '/home';
	}

	public function main_path()
	{
		return self::$app_path . '/main';
	}



}