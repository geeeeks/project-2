<?php

namespace Database;

class Database {

	public $sqlQuery = null;
	public $whereArray = array();
	public $orWhereArray = array();
	public $joinArray = array();
	public static $host;
	public static $user;
	public static $password;
	public static $db;
	public static $connection;
	public static $table;

	public static function configure (
		$host,
		$user,
		$password,
		$db
	)
	{
		self::$host = $host;
		self::$user = $user;
		self::$password = $password;
		self::$db = $db;

		self::$connection = new \mysqli(
			self::$host,
			self::$user,
			self::$password,
			self::$db
		);

		if ( self::$connection->connect_error ) {
			die("Database Connection Failed " . self::$connection->connect_error);
		}
	}

	public static function table($tablename)
	{
		self::$table = $tablename;
		return new static();
	}

	public function insert(Array $array)
	{
		$keys = array();
		$values = array();

		foreach ($array as $key => $value) {
			$keys[] = "`{$key}`";
			$values[] = "'{$value}'";
		}
		$keys = implode(",", $keys);
		$values = implode("," , $values);

		$this->sqlQuery = "INSERT INTO " . self::$table . " ( $keys )" . " VALUES (" .$values .")";

		self::$connection->query($this->sqlQuery);

		$id = self::$connection->insert_id;

		//self::$connection->close();

		return $id;
	}

	public function update(Array $array)
	{
		$str = array();

		foreach ($array as $key => $value) {
			$str[] = "`{$key}` = '{$value}'";
		}

		$str = implode(",", $str);
		$this->sqlQuery = "UPDATE " . self::$table . " SET " . $str . ' ' . $this->getWhere() . $this->getOrWhere();
		return self::$connection->query($this->sqlQuery);
	}

	public function select($column = "*")
	{
		$this->sqlQuery = "SELECT {$column} FROM " . self::$table;
		return $this;
	}

	public function delete()
	{
		$this->sqlQuery = "DELETE FROM " . self::$table . ' ' . $this->getWhere() . $this->getOrWhere();
		return self::$connection->query($this->sqlQuery);
	}

	public function where($column, $condition = '=', $value)
	{
		$str = "`{$column}` {$condition} '{$value}'";
		$this->whereArray[] = $str;
		return $this;
	}

	public function whereIn($column, $value)
	{
		$v = array();

		foreach ($value as $key => $a) {
			array_push($v, "'$a'");
		}

		$im_v = implode(",", $v);

		$str = "`{$column}` IN ( $im_v )";
		$this->whereArray[] = $str;
		return $this;
	}

	public function whereNotIn($column, $value)
	{
		$v = array();

		foreach ($value as $key => $a) {
			array_push($v, "'$a'");
		}

		$im_v = implode(",", $v);

		$str = "`{$column}` NOT IN ( $im_v )";
		$this->whereArray[] = $str;
		return $this;
	}

	public function orWhere($column, $condition = '=', $value)
	{
		$str = "`{$column}` {$condition} '{$value}'";
		$this->orWhereArray[] = $str;
		return $this;
	}

	public function join($columnA, $columnB)
	{
		$str = "`{$columnA}` = `{$columnB}`";
		$this->joinArray[] = $str;
		return $this;
	}

	public function getWhere()
	{
		if( count( $this->whereArray ) ) {
			return " WHERE " . implode(' AND ', $this->whereArray);
		}
	}

	public function getOrWhere()
	{
		if( count( $this->orWhereArray ) ) {
			return " OR " . implode(' OR ', $this->orWhereArray);
		}
	}

	public function getJoin()
	{
		if( count( $this->joinArray ) ) {
			return " FULL OUTER JOIN " . implode(' FULL OUTER JOIN ', $this->joinArray);
		}
	}

	public function get()
	{
		$result = array();

		if( is_null( $this->sqlQuery ) ) {
			$this->sqlQuery = "SELECT * FROM " . self::$table;
		}

		if( count($this->joinArray) ) {
			$this->sqlQuery = $this->sqlQuery . $this->getJoin();
		}

		$result =  self::$connection->query($this->sqlQuery . " {$this->getWhere()} " . " {$this->getOrWhere()} ")->fetch_all(MYSQLI_ASSOC);

		if ( count($result) > 0 ) {
			return $result;
		} else {
			return array();
		}
	}

	public function close()
	{
		return self::$connection->close();
	}

	public function getQuery()
	{
		if( is_null( $this->sqlQuery ) ) {
			$this->sqlQuery = "SELECT * FROM " . self::$table;
		}

		if( count($this->joinArray) ) {
			$this->sqlQuery = $this->sqlQuery . $this->getJoin();
		}

		return $this->sqlQuery . " {$this->getWhere()} " . " {$this->getOrWhere()} ";
	}

	public function find($id)
	{
		if( is_null( $this->sqlQuery ) ) {
			$this->sqlQuery = "SELECT * FROM " . self::$table;
		}

		return self::$connection->query($this->sqlQuery . " WHERE `id` = " . $id)->fetch_assoc();
	}

	public function first()
	{
		if( is_null( $this->sqlQuery ) ) {
			$this->sqlQuery = "SELECT * FROM " . self::$table;
		}

		//echo $this->sqlQuery . " {$this->getWhere()} " . " {$this->getOrWhere()} LIMIT 1";

		//die();

		$result =  self::$connection->query($this->sqlQuery . " {$this->getWhere()} " . " {$this->getOrWhere()} LIMIT 1")->fetch_assoc();



		return $result;
	}


	public static function runSQL($sql)
	{
		return self::$connection->query($sql);
	}

	public static function tableExists($table)
	{
		$table = (array) self::runSQL("SHOW TABLES LIKE '{$table}'")->fetch_assoc();
		if(count($table) > 0) {
			return 1;
		}
		return 0;
	}

	public static function register_hook()
	{
		$isActive = self::table('migration')->select('value')->where('key', '=', 'migrate' )->get();

		if ( count($isActive) ) {
			return 0;
		}

		return 1;
	}

}