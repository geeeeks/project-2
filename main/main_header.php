<!DOCTYPE html>
<html>
    
<!-- Mirrored from limpidthemes-demo.com/Themeforest/html/Pathshala/Pathshala-HTML/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 May 2018 18:37:48 GMT -->
<head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="">
        <title>Pathshala - Responsive Education Template</title>

        <!-- Styles -->
        <link rel="stylesheet" href="<?php assets('css/style.css'); ?>">
        <link rel="stylesheet" href="<?php assets('css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?php assets('css/magnific-popup.css'); ?>">
        <link rel="stylesheet" href="<?php assets('css/owl.carousel.min.css'); ?>">
        <link rel="stylesheet" href="<?php assets('css/owl.theme.default.min.css'); ?>">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="assets/css/owl.carousel.min.css" rel="stylesheet" media="screen">
		<link href="assets/css/owl.theme.default.min.css" rel="stylesheet" media="screen">
        <link href="assets/css/style.css" rel="stylesheet" media="screen">

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
        <link href="assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
	
		<script>
			var ajaxurl = "<?php _e(ajax_url()); ?>" ;
		</script>
    </head>
    <body>
        <div class="row nav-row trans-menu">
            <div class="container nav-container">
				<div class="top-navbar">
					<div class = "pull-right">
						<div class="top-nav-social pull-left">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
								<a href="#"><i class="fa fa-linkedin"></i></a>
						</div>
						<div class="top-nav-login-btn pull-right">
							<a href="#" data-toggle="modal" data-target="#loginModal"><i class="fa fa-sign-in"></i>LOGIN</a>
						</div>
						<!--<div class="top-navbar-search pull-right">
							<i class="fa fa-search"></i>
							<input type = "text" placeholder = "Search"/>
						</div>-->
					</div>
					<div class = "clearfix"></div>
				</div> 
				<div class = "clearfix"></div>
                <nav id="pathshalaNavbar" class="navbar navbar-default" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#pathshalaNavbarCollapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.html">PATHSHALA</a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="pathshalaNavbarCollapse">
                        <ul class="nav navbar-nav">
                            <li><a href="index.html"><i class="fa fa-home"></i>HOME</a></li>
							<li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-graduation-cap"></i>ACADEMICS <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="academic.html"><i class="fa fa-graduation-cap"></i>Faculty of Modern Science</a></li>
                                    <li><a href="academic.html"><i class="fa fa-graduation-cap"></i>Faculty of Arts and Language</a></li>
									<li><a href="academic.html"><i class="fa fa-graduation-cap"></i>Faculty of Social Science</a></li>
									<li><a href="preschool.html"><i class="fa fa-graduation-cap"></i>Faculty of Business Administration</a></li>
                                </ul>
                            </li>
                            <li><a href="admission.html"><i class="fa fa-users"></i>ADMISSIONS</a></li>
							<li><a href="index.html"><i class="fa fa-graduation-cap"></i>IQAC</a></li>
							
							<li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-file"></i>PAGES <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="gallery.html"><i class="fa fa-picture-o"></i>CAMPUS GALLERY</a></li>
									<li><a href="about.html"><i class="fa fa-info-circle"></i>ABOUT</a></li>
                                    <li><a href="contact.html"><i class="fa fa-phone-square"></i>CONTACT US</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav>
            </div>
        </div>

	