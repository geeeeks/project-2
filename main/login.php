<!DOCTYPE html>
<html>
<head>
  <title>Login</title>
</head>
<body>
  <form method="post" id="login" class="form p-4 rounded border">
  <h1 class="pb-4 text-muted">Login</h1>
  <label for="">Email:</label>
  <input class="form-control">
  <br>
  <label for="">Password:</label>
  <input class="form-control" type="password">
  
  <div class="d-flex justify-content-between align-items-center mt-4">
    <a href="" id="showReg">register</a>
    <button class="btn btn-success w-50">Login</button>
  </div>
  
  <div class="border border-bottom-0 border-left-0 border-right-0 mt-3 pt-3">
    <a href="">forget password?</a>
  </div>
</form>
</body>
</html>