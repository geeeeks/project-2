<?php 

if (! function_exists('user_registration') ) {
	function user_registration() {

		
		$message = array();

		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			if(array_key_exists('register', request()->all())){

				if ( ! array_key_exists('name', request()->all()) || ( empty( request()->get('name') ))) {
					$message[] = "User Name is required!";
				}

				if ( ! array_key_exists('nid', request()->all()) || ( empty( request()->get('nid') ))) {
					$message[] = "User nid is required!";
				}

				if ( ! array_key_exists('email', request()->all()) || ( empty( request()->get('email') ))) {
					$message[] = "User email is required!";
				}

				if ( ! array_key_exists('password', request()->all()) || ( empty( request()->get('password') ))) {
					$message[] = "User password is required!";
				}

				if ( ! ( request()->get('password')  === request()->get('confirm_password') ) ) {
					$message[] = "Password Not Match!";
				}

				$checkUserExists = qFluent()->table('user')->where('nid', '=', request()->get('nid'))->first();

				if( $checkUserExists ) {
					$message[] = "User Aleady Exists!";
				}

				if ( count($message) > 0) {
					return $message;
				}				

				$args = array(
					'nid' => request()->get('nid'),
					'user_email' => request()->get('email'),
					'user_name' => request()->get('name'),
					'password' => md5(request()->get('password'))
				);

				$getUserId = qFluent()->table('user')->insert($args);

				qFluent()->table('roles')->insert(array(
					'user_id' => $getUserId,
					'role'	  => 'user'
				));

				$message[] = "Successfully Registration Complete!";

				return $message;
			}
		}

	}
}


if (! function_exists('user_login') ) {

	function user_login() {

		$message = array();

		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			if (array_key_exists('login', request()->all())) {

				if( ! array_key_exists('email', request()->all() )) {

					$message[] = "Email Field Required!";

				} 

				if( ! array_key_exists('password', request()->all()) ) {
					$message[] = "Password Field Required!";
				}

				if ( count( $message ) > 0 )  {
					return $message;
				}

				$checkUser = qFluent()->table('user')->select('id')
													 ->where('user_email', '=', request()->get('email'))
													 ->where('password', '=', md5(request()->get('password')))
													 ->first();
				if ( $checkUser ) {

					$checkAdmin = qFluent()->table('roles')->select('role')
													   ->where('user_id','=',$checkUser['id'])
													   ->first();
					if( $checkAdmin['role'] == 'admin'){
						admin_login_session($checkUser['id']);
						q_redirect('/admin');
					} else if ($checkUser) {
						user_login_session($checkUser['id']);
						q_redirect('/home');
					} else {
						$message[] = "Email or Password Does not match!";
					}
				}

				return $message;

			}

		}


	}

}